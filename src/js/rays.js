var canvas = document.getElementById('canvas'),
	ctx = canvas.getContext('2d'),
	w = canvas.width+10,
	h = canvas.height,
	rays = [],
	full = false,
	tick = 0,
	audioprogress = false,
	tickHueMultiplied;
	Opt = function(){
		this.rays = 30;
		this.maxRadius = Math.sqrt( w*w/2 + h*h/2 );
		this.circleRadiusIncrementAcceleration = 2;
		this.radiantSpan = .4;
		this.rayAngularVelSpan = .005;
		this.rayAngularVelLineWidthMultiplier = 60;
		this.rayAngularAccWaveInputBaseIncrementer = .03;
		this.rayAngularAccWaveInputAddedIncrementer = .02;
		this.rayAngularAccWaveMultiplier = .0003;
		this.baseWaveInputIncrementer = .01;
		this.addedWaveInputIncrementer = .01;
		this.circleNumWaveIncrementerMultiplier = .1;
		this.cx = w / 2;
		this.cy = h / 2;
		this.tickHueMultiplier = 1;
		this.shadowBlur = 0;
		this.repaintAlpha = .2;
		this.apply = startInit
	},
	startInit = function(){
		rays.length = 0;
		for( var i = 0; i < opts.rays; ++i )
			rays.push( new Ray );
		if( tick === 0 ){
			loop();
		}
	},
	loop = function(){
		window.requestAnimationFrame( loop );
		++tick;
		ctx.globalCompositeOperation = 'source-over';
		ctx.shadowBlur = 0;
		ctx.fillStyle = 'rgba(0,0,0,alp)'.replace( 'alp', opts.repaintAlpha );
		ctx.fillRect( 0, 0, w, h );
		ctx.shadowBlur = opts.shadowBlur;
		ctx.globalCompositeOperation = 'lighter';
		tickHueMultiplied = opts.tickHueMultiplier * tick;
		if(audioprogress) {
			rays.map( function( ray ){ ray.step(); } );
		}
	},
	Ray = function(){
		var security = 100,
			count = 0;
		this.circles = [ new Circle( 0 ) ];
		this.rot = Math.random() * Math.PI * 2;
		this.angularVel = Math.random() * opts.rayAngularVelSpan * ( Math.random() < .5 ? 1 : -1 );
		this.angularAccWaveInput = Math.random() * Math.PI * 2;
		this.angularAccWaveInputIncrementer = opts.rayAngularAccWaveInputBaseIncrementer + opts.rayAngularAccWaveInputAddedIncrementer * Math.random();
		while( --security > 0 && this.circles[ count ].radius < opts.maxRadius )
			this.circles.push( new Circle( ++count ) );
	},
	Circle = function( n ){
		this.radius = opts.circleRadiusIncrementAcceleration * Math.pow( n, 2 );
		this.waveInputIncrementer = ( opts.baseWaveInputIncrementer + opts.addedWaveInputIncrementer * Math.random() ) * ( Math.random() < .5 ? 1 : -1 ) * opts.circleNumWaveIncrementerMultiplier * n;
		this.waveInput = Math.random() * Math.PI * 2;
		this.radiant = Math.random() * opts.radiantSpan * ( Math.random() < .5 ? 1 : -1 );
	};
	
Ray.prototype.step = function(){
	// this is just messy, but if you take your time to read it properly you'll understand it pretty easily
	this.rot += 
		this.angularVel += Math.sin( 
			this.angularAccWaveInput += 
				this.angularAccWaveInputIncrementer ) * opts.rayAngularAccWaveMultiplier;
	
	var rot = this.rot,
			x = opts.cx,
			y = opts.cy;
	
	ctx.lineWidth = Math.min( .00001 / Math.abs( this.angularVel ), 10 / opts.rayAngularVelLineWidthMultiplier ) * opts.rayAngularVelLineWidthMultiplier;

	ctx.beginPath();
	ctx.moveTo( x, y );
	
	for( var i = 0; i < this.circles.length; ++i ){
		
		var circle = this.circles[ i ];
		
		circle.step();
		
		rot += circle.radiant;
		
		var x2 = opts.cx + Math.sin( rot ) * circle.radius,
				y2 = opts.cy + Math.cos( rot ) * circle.radius,
				
				mx = ( x + x2 ) / 2,
				my = ( y + y2 ) / 2;
		
		ctx.quadraticCurveTo( x, y, mx, my );
		
		x = x2;
		y = y2;
	}
	
	ctx.strokeStyle = ctx.shadowColor = 'hsl(hue,80%,50%)'.replace( 'hue', ( ( ( rot + this.rot ) / 2 ) % ( Math.PI * 2 ) ) / Math.PI * 30 + tickHueMultiplied );
	
	ctx.stroke();
}
Circle.prototype.step = function(){
	
	this.waveInput += this.waveInputIncrementer;
	this.radiant = Math.sin( this.waveInput ) * opts.radiantSpan;
}
opts = new Opt();