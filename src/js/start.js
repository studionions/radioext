(function(){
	"use strict";
	window.styleSheetWriter = (
		function () {
			var selectorMap = {},
				supportsInsertRule;	
			return {
				getSheet: (function () {
					var sheet = false;
					return function () {
						if (!sheet) {
							var style = document.createElement("style");
							style.appendChild(document.createTextNode(""));
							style.title = "radioextStyleSheet";
							document.head.appendChild(style);
							sheet = style.sheet;
							supportsInsertRule = (sheet.insertRule == undefined) ? false : true;
						};
						return sheet;
					}
				})(),
				setRule: function (selector, property, value) {
					var sheet = this.getSheet(),
						rules = sheet.rules || sheet.cssRules;
					property = property.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();});
					if (!selectorMap.hasOwnProperty(selector)){
						var index = rules.length;
						if (supportsInsertRule) {
							sheet.insertRule([selector, " {", property, ": ", value, ";}"].join(""), index);
						} else {
							sheet.addRule(selector, [property, ": ", value].join(""), index);
						}
						selectorMap[selector] = index;
					} else {
						rules[selectorMap[selector]].style.setProperty(property, value);
					}
				},
				clear: function(){
					var sheet = this.getSheet();
					if(sheet.rules.length){
						while(sheet.rules.length){
							sheet.deleteRule(0);
						}
					}
					selectorMap = [];
				}
			};
		}
	)();
	var custom = [
					{
						"selector" : "#optionPage .skin_custom",
						"css" : {
							"background-image": "gradient",
						}
					},
					{
						"selector" : "#optionPage .skin_custom .main-wrapp",
						"css" : {
							"border-left-color" : "backgroundHeader",
							"border-right-color" : "backgroundHeader",
						}
					},
					{
						"selector" : ".skin_custom .main",
						"css" : {
							"background-color"	: "backgroundBody",
							"border-left-color" : "backgroundHeader",
							"border-right-color" : "backgroundHeader",
						}
					},
					{
						"selector" : ".skin_custom .header_background",
						"css" : {
							"fill"	: "backgroundHeader"
						}
					},
					{
						"selector" : "#counter, .skin_custom .track span, .skin_custom .shareds_item > span.description",
						"css" : {
							"color"	: "color"
						}
					},
					{
						"selector" : ".skin_custom .header .icon:hover, .main .header_canvas .radio_block .rlogo",
						"css" : {
							"color"	: "backgroundHeader"
						}
					},
					{
						"selector" : ".skin_custom .jspTrack .jspDrag",
						"css" : {
							"background-color"	: "backgroundBody"//"drag"
						}
					},
					{
						"selector" : ".skin_custom input[type=range]::-webkit-slider-thumb, .skin_custom .scroll li .button:hover, .skin_custom .scroll li .favicon:hover, .skin_custom .scroll li.active .button, .skin_custom .scroll li.favorite .favicon, .scroll li span.new_title, .skin_custom .track, .skin_custom .footer",
						"css" : {
							"background-color"	: "backgroundHeader"
						}
					},
					{
						"selector" : ".skin_custom input[type=range], .skin_custom .scroll .logo",
						"css" : {
							"border-color"		: "backgroundHeader"
						}
					},
					{
						"selector" : ".skin_custom .scroll li",
						"css" : {
							"border-color"		: "backgroundBody"
						}
					}
				];
	
	window.styleChange = function(){
		var d = [],
			a = document.getElementsByTagName("body")[0],
			c = window.options.isSchemeDefault ? window.options.schemeDefaultIndex : "custom",
			e = document.getElementById("color"),
			b = document.getElementById("optskn"),
			f = document.getElementById("optionPage");
		(a.getAttribute("id")=="popup") && d.push("popup");
		d.push("skin_"+c);
		b != null && (b.value = c);
		a.className = d.join(" ");
		styleSheetWriter.clear();
		e != null && (e.style.display = "none");
		if(c=="custom"){
			e != null && (e.style.display = "block");
			var car = window._helper.hex2rgb(window.options.customColor),
				colors = window._helper.setColors(car.r, car.g, car.b);
			colors.color = window.options.color;
			custom.forEach(function(element, index, array){
				var selector = "";
				for(var key in element){
					if(key=="selector"){
						selector = element[key];
					}
					if(key=="css"){
						var ccsObj = element[key];
						for(var css in ccsObj){
							styleSheetWriter.setRule(selector,css,colors[ccsObj[css]])
						}
					}
				}
			})
		}
		if(f != null){
			window.options.showPanel && f.classList.add("open");
		}
	}
	window.styleChange();
}());