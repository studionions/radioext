;(function(window, undefined){
	"use strict";
	window.stations = [];
	window.count = false;
	window.search = "";
	window.installDom = "http://rds.my/";
	window.trackInfo = lang.get("name");
	window.player = {};
	var webstores = ["ngcpadnngcnkkdfhckclpdpimifikmpa", "kmfiemllajnpacikiioecokiepdahjmo"],
		manifest = chrome.runtime.getManifest(),
		extension = chrome.runtime.id,
		apiUri = window.installDom + "api.json?",
		warningId = 'radioext.notifications_'+extension,
		_isPlaying = false,
		_userChange = false,
		_isOnline = false,
		_isProgress = false,
		reqInt, infoXnr, xnr, extInt, audioWait,
		countUser = 0,
		record = 0,
		hideWarning = function(done) {
			if(window.options.showNotify){
				chrome.notifications.clear(warningId, function() {
					done && done();
				});
			}
		},
		playerHandle = function(e){
			var ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
			switch(e.type){
				case "networkchange":
					_isOnline = window.navigator.onLine;
					var ttl = ind>-1 ? window.stations[ind].title : lang.get('name');
					!_isOnline && (
						player.stop(),
						chrome.browserAction.setTitle({title: ttl}),
						window.count = false,
						sendMessage('trackInfo', {title: ttl, count: 0, record: 0})
					);
					_userChange && _isOnline && (player.play(),getInfo());
					break;
				case "volumechange":
					sendMessage("volumechange", e);
					break;
				case "statechange":
					if(e.audioev=="progress"){
						sendMessage("progress", e);
					}else if (e.audioev=="play" || e.audioev=="playing"){
						sendMessage("play", e);
					}else if (e.audioev=="stop"){
						sendMessage("stop", e);
						var ttl = ind>-1 ? window.stations[ind].title : lang.get('name');
						chrome.browserAction.setTitle(
							{
								title:  lang.get("name") +"\r\n"+ ( ind>-1 ? ttl : "" )
							}
						);
					}
					break;
			}
		},
		init =  function(){
			if(/^https:\/\/clients2\.google\.com\//.test(manifest.update_url) && extension=="ngcpadnngcnkkdfhckclpdpimifikmpa") {
				installDom = "https://chrome.google.com/webstore/detail/ngcpadnngcnkkdfhckclpdpimifikmpa/reviews";
			};
			if(/^https:\/\/extension-updates\.opera\.com\//.test(manifest.update_url) && extension=="kmfiemllajnpacikiioecokiepdahjmo") {
				installDom = "https://addons.opera.com/extensions/details/app_id/kmfiemllajnpacikiioecokiepdahjmo/#feedback-container";
			}
			player = new AudioPlayer();
			player.volume = window.options.volume;
			player.addEventListener('volumechange', playerHandle);
			player.addEventListener('statechange', playerHandle);
			player.addEventListener('networkchange', playerHandle);
			getStationsXhr();
		},
		getStationsXhr = function() {
			reqInt && clearTimeout(reqInt);
			extInt && clearTimeout(extInt);
			window.stations = [];
			chrome.browserAction.setBadgeBackgroundColor({color:[79, 87, 255, 255]});
			chrome.browserAction.setBadgeText({text: 'Load'});
			try{
				xnr.onload = null;
				xnr.onerror = null;
				xnr.abort();
				xnr = null;
			}catch(e){};
			xnr = new XMLHttpRequest();
			xnr.open('GET',apiUri+"_="+(new Date()).getTime(), true);
			setRequestHeader(xnr);
			xnr.onload = function(){
				var data;
				try{
					data = JSON.parse(this.response);
					parseJson.apply(player,	[data]);
					chrome.browserAction.setBadgeText({text: ''});
					sendMessage('complete');
				}catch(e){
					chrome.browserAction.setBadgeBackgroundColor({color:[255, 79, 87, 255]});
					chrome.browserAction.setBadgeText({text: 'Error'});
					// Перезапуск через 5 секунд
					extInt = setTimeout(function(){getStationsXhr();}, 5000);
				}
			};
			xnr.onerror = function(){
				clearTimeout(extInt);
				chrome.browserAction.setBadgeBackgroundColor({color:[255, 79, 87, 255]});
				chrome.browserAction.setBadgeText({text: 'Error'});
				// Перезапуск через 5 секунд
				extInt = setTimeout(function(){getStationsXhr();}, 5000);
			};
			xnr.send();
		},
		setRequestHeader = function(xr){
			xr.setRequestHeader('X-Requested-With', "XMLHttpRequest");
			xr.setRequestHeader('extension', extension);
			xr.setRequestHeader('Accept', 'application/json, text/javascript, */*; q=0.01');
		},
		parseJson = function(data){
			reqInt && clearTimeout(reqInt);
			window.stations = [];
			for(var i = 0; i< data.length; ++i){
				var val = window.stations[i] = data[i];
				if(window.options.playStation==val.alias){
					window.trackInfo = val.title;
					chrome.browserAction.setTitle({title: lang.get("name")+"\r\n"+val.title});
				}
			}
		},
		sendMessage = function(action, data) {
			chrome.runtime.sendMessage({
				name: "popup",
				action: action,
				data: "undefined" != typeof data ? data : null
			})
		},
		getArrayIndex = function (element, index, array) {
			if(element.alias==this.alias){
				return true;
			}
			return false;
		},
		sucessInfo = function(){
			reqInt && clearTimeout(reqInt);
			var data, ind, notify = "radioext.notifications";
			try{
				data = JSON.parse(this.response);
				ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
				if(player.isPlaying()){
					var title = (data.title != null || data.title != undefined) ? (data.title.length > 5 ? data.title : window.stations[ind].title) : window.stations[ind].title;
					if(title != window.stations[ind].title && title != window.trackInfo){
						hideWarning(function() {
						chrome.notifications.create(warningId, {
							iconUrl: window.stations[ind].logo,
							title: window.stations[ind].title,
							type: 'basic',//'image'
							message: "Сейчас играет:\n"+title,
							isClickable: true,
							priority: 2,
							}, function() {setTimeout(hideWarning, 5000);});
						});
					}
					window.trackInfo = title;
					countUser = data.count;
					record = data.record;
					window.count = {
						count: data.count,
						record: data.record
					};
					chrome.browserAction.setTitle({title: window.stations[ind].title+"\r\n"+window.trackInfo});
					sendMessage('trackInfo', {title:title, count: countUser, record: record});
					reqInt = setTimeout(getInfo, 15000);
				}else{
					chrome.browserAction.setTitle({title: window.stations[ind].title});
					window.count = false;
					sendMessage('trackInfo', {title:window.stations[ind].title, count: countUser, record: record});
				}
			}catch(e){
				window.count = false;
				sendMessage('trackInfo', {title:window.stations[ind].title, count: countUser, record: record});
			}
		},
		errorInfo = function(){
			reqInt && clearTimeout(reqInt);
			var ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
			sendMessage('trackInfo', {title:window.stations[ind].title, count: countUser, record: record});
			if(player.isPlaying()){
				reqInt = setTimeout(function(){getInfo()}, 15000);
			}
		},
		getInfo = function(){
			reqInt && clearTimeout(reqInt);
			try{
				infoXnr.onload = null;
				infoXnr.onerror = null;
				infoXnr.abort();
				infoXnr = null;
			}catch(tr){}
			infoXnr = new XMLHttpRequest();
			infoXnr.open('GET', apiUri+"alias="+window.options.playStation, true);
			setRequestHeader(infoXnr);
			infoXnr.onload = sucessInfo;
			infoXnr.onerror = errorInfo;
			infoXnr.send();
		},
		clipboardTrackInfo = function(meintext){
			var ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
			if(ind>-1){
				var titleStation = window.stations[ind].title;
				if(titleStation != meintext){
					var copyFrom = document.createElement('textarea');
					copyFrom.textContent = meintext;
					document.body.appendChild(copyFrom);
					copyFrom.select();
					document.execCommand('copy');
					document.body.removeChild(copyFrom);
					hideWarning(function() {
						chrome.notifications.create(warningId, {
							iconUrl: window.stations[ind].logo,
							title: window.stations[ind].title,
							type: 'basic',//'image'
							message: "Скопировано:\n"+meintext,
							isClickable: true,
							priority: 2,
						}, function() {setTimeout(hideWarning, 5000)});
					});
				}
			}
			return false;
		},
		searchTrack = function(meintext, search){
			var ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
			if(ind>-1){
				var titleStation = window.stations[ind].title,
					url = "about://about";
				if(titleStation != meintext){
					meintext = encodeURIComponent(meintext);
					chrome.tabs.create({
						url: ((search=="yandex") ? "https://www.yandex.ru/yandsearch?text=":"https://www.google.ru/search?q=")+meintext,
						active: true
					})
				}
			}
			return false;
		},
		sendPopupMeaasge = function(a) {
			if(a.name == "background"){
				switch(a.action){
					case "play":
						var ind = window.stations.findIndex(getArrayIndex, {alias:a.data.currentTrack});
						if(ind>-1){
							_userChange = true;
							if(player.stream != window.stations[ind].stream) {
								window.options.playStation = a.data.currentTrack;
								window.trackInfo = a.data.title;
								player.stream = window.stations[ind].stream;
								window.options.playid = window.stations[ind].id;
							}
							player.play();
							sendMessage('trackInfo', {title:window.trackInfo, count: 0, record: 0});
							getInfo();
							return;
						}
						window.trackInfo = lang.get("name");
						player.stop();
						reqInt && clearTimeout(reqInt);
						sendMessage('trackInfo', {title:window.trackInfo, count: 0, record: 0});
						_userChange = false;
						break;
					case "stop":
						player.stop();
						reqInt && clearTimeout(reqInt);
						var ind = window.stations.findIndex(getArrayIndex, {alias:window.options.playStation});
						_userChange = false;
						if(ind>-1){
							var st = window.stations[ind];
							window.trackInfo = st.title;
						}else{
							window.trackInfo = lang.get("name");
						}
						sendMessage('trackInfo', {title:window.trackInfo, count: 0, record: 0});
						window.count = false;
						break;
					case "volume":
						player.volume = window.options.volume;
						break;
					case "search":
						search = a.data.search
						sendMessage("search", {search: search});
						break;
					case "reload":
						var ttl = ind>-1 ? window.stations[ind].title : lang.get('name');
						chrome.browserAction.setTitle({title: ttl});
						window.count = false;
						sendMessage('trackInfo', {title: ttl, count: 0, record: 0});
						chrome.browserAction.setTitle(
							{
								title:  lang.get("name") +"\r\n"+ ( ind>-1 ? ttl : "" )
							}
						);
						getStationsXhr();
						break;
					case "copyTrackInfo":
						clipboardTrackInfo(window.trackInfo)
						break;
					case "searchTrack":
						searchTrack(window.trackInfo, a.data.search);
						break;
				}
			}
		};
	chrome.runtime.onMessage.addListener(sendPopupMeaasge);
	chrome.commands.onCommand.addListener(function(command) {
		var val = player.volume;
		switch(command){
			case "reload_stations":
				//reloadData();
				break;
			case "playstop":
				var ind;
				player.isPlaying() ? (
					sendPopupMeaasge({
						name: "background",
						action: "stop",
						data: null
					})
				) : (
					(window.options.playStation != "false") && (
						sendPopupMeaasge({
							name: "background",
							action: "play",
							data: {
								currentTrack: window.options.playStation,
								//title: window.stations[ind].title
							}
						})
					)
				)
				break;
			case "volumeup":
				player.volume += 0.05;
				break;
			case "volumedown":
				player.volume -= 0.05;
				break;
		}
	});
	init();
}(window))