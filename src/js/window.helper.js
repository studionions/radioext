Math.inInterval = Math.inInterval || function(val, min, max){
	return Math.min(max, Math.max(min, val));
}
window.lang = {
	get: function(val){
		return chrome.i18n.getMessage(val);
	}
};
window._helper = {
	pack: function(data){
		return JSON.stringify(data);
	},
	unpack: function(data){
		try {
			return JSON.parse(data);
		} catch(ex) {
			return null;
		}
	},
	isNumber: function(value){
		return (!isNaN(parseFloat(value)) && isFinite(value) && !this.isString && !this.isBoolean && !this.isObject && !this.isArray);
	},
	isArray: function (value){
		return (!this.isNull(value) && (Object.prototype.toString.call(value) === '[object Array]'));
	},
	isObject: function(value){
		return (!this.isEmpty(value) && (typeof value == 'object'));
	},
	isBoolean: function(value){
		return (typeof value == 'boolean');
	},
	isString: function(value){
		return (typeof value == 'string');
	},
	isNull: function(value){
		return ((value === undefined) || (value === null));
	},
	isEmpty: function(value){
		return ( this.isNull(value) || ((typeof value.length != 'undefined') && (value.length == 0)) );
	},
	isLight: function (r, g, b) {
		return (0.213 * r + 0.715 * g + 0.072 * b > 255 / 2);
	},
	hex2rgb: function(value){
		var cl = parseInt(value.slice(1),16),
			R  = cl >> 16,
			G  = cl >> 8 & 0x00FF,
			B  = cl & 0x0000FF;
		return {r:parseInt(R), g:parseInt(G), b:parseInt(B)};
	},
	rgb2hex: function(r, g, b){
		return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},
	setColors: function(r, g, b) {
		var bbR = Math.inInterval(Math.round(r+(255-r)*0.4),0,255),
			bbG = Math.inInterval(Math.round(g+(255-g)*0.4),0,255),
			bbB = Math.inInterval(Math.round(b+(255-b)*0.4),0,255),
			bgR = Math.inInterval(Math.round(r+(255-r)*0.55),0,255),
			bgG = Math.inInterval(Math.round(g+(255-g)*0.55),0,255),
			bgB = Math.inInterval(Math.round(b+(255-b)*0.55),0,255);
		return {
			backgroundBody: this.rgb2hex(bbR, bbG, bbB),
			backgroundHeader: this.rgb2hex(r, g, b),
			color: this.isLight(bbR, bbG, bbB) ? "#000000" : "#ffffff",
			gradient: 'radial-gradient(circle, '+this.rgb2hex(bgR, bgG, bgB)+', '+this.rgb2hex(r, g, b)+')'
		}
	},
	in_array: function(value, array) 
	{
		for(var i = 0; i < array.length; i++) 
		{
			if(array[i] == value) return true;
		}
		return false;
	},
	get defaultSchemes(){
		return [
			{
				name: "Tints of Gainsboro"
			},
			{
				name: "Tints of Safety Orange *(Default)"
			},
			{
				name: "Tints of Deep Sky Blue"
			},
			{
				name: "Tints of Fruit Salad"
			},
			{
				name: "Tints of Free Speech Magenta"
			},
			{
				name: "Tints of Sunset Orange"
			},
			{
				name: "Tints of Golden Yellow"
			}
		]
	},
	set defaultSchemes(val){
		throw new TypeError("window._helper.defaultSchemes read-only property");
	}
};
window.storage = {
	get: function(key, defValue, typeValue){
		var val = (typeof localStorage[key] == "undefined" || typeof localStorage[key] == "null") ? _helper.pack(defValue) : localStorage[key];
		val = _helper.unpack(val) == null ? defValue : _helper.unpack(val);
		switch (typeValue){
			case 'number':
				if(_helper.isNull(val) || _helper.isEmpty(val))
					val = 0;
				if(!_helper.isNumber(val))
					val = parseFloat(val);
				this.set(key, val);
				break;
			case 'boolean':
				val = (_helper.isBoolean(val)) ? val : (defValue ? true : false);
				this.set(key, val);
				break;
			case 'object':
			case 'array':
				val = _helper.isObject(val) || _helper.isArray(val) ? val : (typeValue == "array" ? [] : {});
				this.set(key, val);
				break;
			default:
				this.set(key, val);
				break;
		}
		return val;
	},
	set: function(key, value){
		value = _helper.pack(value);
		localStorage[key] = value;
	},
	reset: function(){
		localStorage.clear();
	}
};
window.options = {
	// Getters 
	get showNotify(){return storage.get("show_notify", true, 'boolean')},
	get showPanel(){return storage.get("showPanel", true, "boolean")},
	get showCount(){return storage.get("showCount", true, "boolean")},
	get volume(){return storage.get("volume", 0.5, 'number')},
	get playStation(){return storage.get("playStation", "false")},
	get playid(){return storage.get("playid", -1, "number")},
	get trackInfo(){return storage.get("trackInfo", "false")},
	get favorite(){return storage.get("favorite", [], 'array')},
	get scheme(){return storage.get("scheme", window._helper.defaultSchemes[0], "object")},
	get isSchemeDefault(){return storage.get("isSchemeDefault", true, "boolean")},
	get schemeDefaultIndex(){return storage.get("schemeDefaultIndex", 1, "number")},
	get customColor(){return storage.get("customColor", "#525252")},
	get color(){return storage.get("color", "#ffffff")},
	// Setters 
	set showNotify(val){storage.set("show_notify",val)},
	set showPanel(val){storage.set("showPanel", val)},
	set showCount(val){storage.set("showCount", val)},
	set volume(val){storage.set("volume",val)},
	set playStation(val){storage.set("playStation", val)},
	set playid(val){storage.set("playid", val)},
	set trackInfo(val){storage.set("trackInfo", val)},
	set favorite(val){storage.set("favorite", val)},
	set scheme(val){storage.set("scheme", val)},
	set isSchemeDefault(val){storage.set("isSchemeDefault", val)},
	set schemeDefaultIndex(val){storage.set("schemeDefaultIndex", val)},
	set customColor(val){storage.set("customColor", val)},
	set color(val){storage.set("color", val)}
};
// Awesomplete
// https://github.com/LeaVerou/awesomplete/blob/gh-pages/awesomplete.js#L435
// Make sure to export Awesomplete on self when in a browser
window.self = window;