var picker, bgpage;
$(function(){
	"use strict";
	var themes = window._helper.defaultSchemes;
	$("#optskn option").each(function(){
		var v = $(this).attr("value");
		if(v != "custom"){
			$(this).empty().text(themes[v].name);
		}else{
			$(this).empty().text("* User theme");
		}
	});
	var api,
		reload = false,
		manifest = chrome.runtime.getManifest(),
		isPlaying = false,
		volumeFocus = false,
	init = function(){
		chrome.runtime.onMessage.addListener(sendPopup);
		var arl = 0,
			brl = 0,
			crl = 20,
			$track = $('.track'),
			
			$span = $('span', $track),
			timer_track=0;
		$('.track').hover(
			function (e){
				arl = $span.width() - $span.parent().width();
				brl = Math.floor(-$span.css('marginLeft').split('px')[0]);
				(arl>=0) && $span.stop().animate({'margin-left': -arl}, crl * (arl - brl), 'linear').parent().css({'text-overflow': 'initial'});
			},
			function (e) {
				brl = Math.floor(-$span.css('marginLeft').split('px')[0]);
				brl>=0 ? $span.stop().animate({'margin-left': 0} , crl * brl, 'linear', function(){$span.parent().css({'text-overflow': 'ellipsis'})}) : $span.parent().css({'text-overflow': 'ellipsis'});
			}
		);
		$("#radioext_volume").on("input change", function(){
			var val = parseFloat($(this).val());
			if(window.options.volume != val){
				window.options.volume = val;
				sendMessage("volume");
			}
			
		}).on("mousewheel", function(e){
			var val = window.options.volume;
			e.deltaY > 0 ? val -= 0.01 : val += 0.01;
			window.options.volume = val;
			sendMessage("volume");
		}).on("mousedown", function(e){
			volumeFocus = true;
		}).val(window.options.volume);
		$(document).on("mouseup", function(e){
			volumeFocus = false;
		});
		setData(bgpage.stations, true);
		$('header .rlogo').on("click", function(e){
			e.preventDefault();
			$(this).hasClass('station') && (
				$(this).hasClass('play') ? sendMessage("stop") : ($(this).attr('data-playing-logo') && sendMessage("play", {currentTrack: $(this).attr('data-playing-logo')}))
			)
			return !1;
		});
		setTimeout(function(){startInit();loop();$.contextMenu(optionsContext);}, 0);
		viewCount();
	},
	sendPopup = function(a) {
		if(a.name == "popup"){
			switch(a.action){
				case "complete":
					setData(bgpage.stations, true);
					break;
				case "play":
					var cur = window.options.playStation,
						activeCur = $(".radiolist ul li.active").data("alias");
					if(cur != activeCur){
						var $li = $('li[data-alias='+cur+']',$('ul'));
						$(".radiolist ul li").removeClass("active play progress");
						$li.addClass("active");
						$('header .rlogo')
							.css({'background-image':'url("'+$li.data('station').logo+'")',cursor:"pointer"})
							.attr('data-playing-logo', $li.data('station').alias)
					}
					if(a.data.bufering){
						audioprogress = false;
						$(".radiolist ul li.active").addClass("progress");
					}else{
						audioprogress = true;
						$(".radiolist ul li.active").removeClass("progress");
					}
					if(a.data.playing){
						audioprogress = true;
						!$(".radiolist ul li.active").hasClass("play") && $(".radiolist ul li.active").addClass("play");
						!$('header .rlogo').hasClass("play") && $('header .rlogo').addClass("play");
					}else{
						audioprogress = false;
						$(".radiolist ul li.active, header .rlogo").removeClass("play")
					}
					break;
				case "stop":
					audioprogress = false;
					$(".radiolist ul li.active, header .rlogo").removeClass("progress play");
					break;
				case "volumechange":
					if(!volumeFocus){
						$("#radioext_volume").val(a.data.volume);
					}
					break;
				case "search":
					($("#radioext_search").val() != a.data.search) && ($("#radioext_search").val(a.data.search));
					searchStation.call($("#radioext_search")[0]);
					$("#radioext_search").val().length ? $("#search-clear").addClass("active") : $("#search-clear").removeClass("active")
					break;
				case "progress":
					if(a.data.bufering){
						audioprogress = false;
						$(".radiolist ul li.active").addClass("progress");
					}else{
						audioprogress = true;
						$(".radiolist ul li.active").removeClass("progress");
					}
					break;
				case "stylechange":
					if(picker){
						if(window.options.customColor != picker.toHEXString()){
							picker.fromString(window.options.customColor);
						}
					}
					window.styleChange();
					break;
				case "favoritechange":
					setTimeout(function(){
						setData(bgpage.stations, false);
					}, 0);
					break;
				case "reload":
					reloadStations();
					break;
				case "fullscreen":
					if(!document.webkitIsFullScreen){
						canvas.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
					}else{
						document.webkitCancelFullScreen();
						document.webkitExitFullscreen();
					}
					break;
				case "trackInfo":
					$("#tracking").text(a.data.title);
					$('.track').trigger("mouseout");
					$.contextMenu(optionsContext);
					viewCount();
					break;
				case "showCount": 
					window.options.showCount ? $("#counter").removeClass("hidden") : $("#counter").addClass("hidden");
					break;
			}
		}
	},
	sendMessage = function(action, data){
		chrome.runtime.sendMessage({
            name: "background",
            action: action,
            data: "undefined" != typeof data ? data : null
        })
	},
	drawItem = function(data, favs){
		var li = $('<li></li>').data({
				'alias'				: data.alias,
				'stream'			: data.stream,
				'station-number'	: data.id,
				'station-name'		: data.title,
				'station'			: data
			}).attr({
				'data-id': data.id,
				'data-alias': data.alias
			}),
			d = $('<div></div>'),
			b = $('<span></span>',{'class':'button'}).data({
				"creator" : li
			}),
			s = $('<span></span>',{'class':'radiotitle'}).data('new',data.title).append($("<i>", {text: data.title})),
			img = $('<img />', {
				'class': 'lazy',
				'src': 'assets/images/lazy.png',
				'data-original':data.logo,
				'width': '32',
				'height': '32'
			}),
			f = $('<span></span>', {
					'class': 'favicon'
				}).data({
					"creator" : li
				}).on('click', clickFavicon).attr({"title":(favs ? lang.get("removeFavorite") : lang.get("addFavorite"))}),
			i = $('<span></span>', {'class':'logo'}).data({
				"creator" : li
			}).append(img),
			playStaion = (window.options.playStation == data.alias);
		li.append(d.append(b).append(i).append(s).append(f));
		data.newst && li.addClass("new").append(
			$("<span></span>", {
				class: "new_title",
				text: lang.get("newstation")
			})
		);
		favs && li.addClass("favorite");
		playStaion && (
			li.addClass("active"),
			isPlaying && (li.addClass("play"), $('header .rlogo').addClass('play'), audioprogress = true),
			bgpage.player.isProgress() && li.addClass("progress"),
			$('header .rlogo').css({'background-image':'url("'+data.logo+'")',cursor:"pointer"})
								.attr('data-playing-logo', data.alias).addClass('station')
		);
		return li;
	},
	getIndex = function (element, index, array) {
		if(element.alias==this.alias){
			return true;
		}
		return false;
	},
	setData = function(data, src){
		src = src ? true : false;
		isPlaying = bgpage.player.isPlaying();
		var ulFn = $('<ul></ul>',{'class':'favList'}),
			ulSt = $('<ul></ul>',{'class':'staList'}),
			dataFav = window.options.favorite,
			v = 0,
			fv = [],
			_fv = [],
			st = [],
			stn = [],
			acs = [];
		$('.scroll').empty().append(ulFn).append(ulSt);
		if(window._helper.isArray(data)){
			$(data).each(function(key, val){
				acs.push(val.title);
				val.favorite = false;
				var indFav = dataFav.findIndex(getIndex, {alias: val.alias});
				if(indFav>-1){
					val.favorite = true;
					fv[indFav] = val;
				}else{
					st.push(val);
					ulSt.append(drawItem(val, false));
				};
			});
			$.each(
				fv.filter(
					function(e){
						return ('id' in e && typeof(e.id) === 'number' && !isNaN(e.id))
					}
				),
				function(key, val){
					ulFn.append(drawItem(val, true));
				}
			);
			awesomplete.list = acs;
		}
		if(!api){
			api = $('.radiolist').jScrollPane({
				contentWidth: 279,
				verticalDragMinHeight: 56
			}).data('jsp');
		}
		if(src){
			setTimeout(function(){
				!api && (api = $('.radiolist').jScrollPane({
					contentWidth: 279,
					verticalDragMinHeight: 56
				}).data('jsp'));
				$('li.active', $('.scroll')).length && (api.scrollTo(0, parseInt($('li.active', $('.scroll')).position().top)));
				onSortable();
				$(window).trigger('resize');
			}, 10);
		}
		onSortable();
		setTimeout(function(){
			$('.radiolist img').lazyload({
				effect : "fadeIn"
			});
			$("#radioext_search").val(bgpage.search).trigger("change");
			$(window).trigger('resize');
		}, 10);
	},
	searchStation = function(){
		var pattern = $(this).val().trim().toLowerCase();
		$('li', '.radiolist').each(function(){
			var str = $(".radiotitle", this).text().toLowerCase(),
				indexSearch = str.indexOf(pattern);
				(indexSearch > -1) ? $(this).removeClass("hide") : (!$(this).hasClass('active') && $(this).addClass("hide"));
		});
		api && api.reinitialise(
			{
				contentWidth: 279,
				verticalDragMinHeight: 56
			}
		);
		$(window).trigger('resize');
	},
	clickFavicon = function(e){
		var $p = $(this).data("creator"),
			data = $p.data('station');
		$p.hasClass('favorite') ? ($p.removeClass('favorite'), data.favorite = false, $p.data('station', data)) : ($p.addClass('favorite'), data.favorite = true, $p.data('station', data));	
		saveFavoriteStations();
	},
	clickStation = function(e){
		var pr = $(this).data("creator");
		$('.radiolist ul li').removeClass("progress");
		!pr.hasClass("active") && ($('.radiolist ul li').removeClass('active play'), pr.addClass('active') /* is playing */);
		var station = pr.data("station");
		pr.hasClass("play") ? (pr.removeClass("play"), $('header .rlogo').removeClass('play'), sendMessage("stop")) : (pr.addClass("play progress"), $('header .rlogo').addClass('play station') ,sendMessage("play", {currentTrack: station.alias}));
		$('header .rlogo').css({'background-image':'url("'+station.logo+'")',cursor:"pointer"})
								.attr('data-playing-logo', station.alias);
	},
	saveFavoriteStations = function(){
		sortableFavoriteStations();
		sendPopup({
			name: "popup",
			action: "favoritechange",
			data: "undefined" != typeof data ? data : null
		})
	},
	sortableFavoriteStations = function(){
		var favoriteList = [];
		$.each($('.favorite'), function(key, val){
			favoriteList.push($(val).data('station'));
		});
		window.options.favorite = favoriteList;
	},
	onSortable = function(){
		$('ul.favList').sortable(
			{
				delay:0,
				tolerance: 'pointer', 
				containment: 'parent', 
				axis:'y',
				cursor: 'row-resize', 
				distance: 20, 
				stop:sortableFavoriteStations
			}
		);
		$("ul").disableSelection();
		
	},
	copyTrack = function(){
		var stitle = bgpage.player.isPlaying() ? $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title : "not",
			trackTitle = bgpage.trackInfo;
		if(stitle != trackTitle){
			sendMessage('copyTrackInfo');
		}
	},
	reloadStations = function(){
		reload = true;
		sendMessage("stop");
		awesomplete.list = [];
		$('.radiolist .scroll *').remove();
		$('#tracking').text(lang.get('name'));
		$('header .rlogo').removeClass("station play").removeAttr('data-playing-logo style');
		awesomplete.list = [];
		$(window).trigger("resize");
		sendMessage("reload");
	};
	var contextitems = $("body").hasClass("popup") ? {
		"reload": {
			name: lang.get('reload'),
			icon: "reload",
			disabled: false
		},
		"copy": {
			name: lang.get('clipboardItem'),
			icon: "copy",
			disabled: function(key, options){
			if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"search-google": {
			name: lang.get('searchGoogle'),
			icon: "web-search",
			disabled: function(key, options){
				if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"search-yandex": {
			name: lang.get('searchYandex'),
			icon: "web-search",
			disabled: function(key, options){
				if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"sep_02":"-----------------",
		"reset": {
			name: lang.get('menuReset'),
			icon: "reset"
		}
	} : {
		"reload": {
			name: lang.get('reload'),
			icon: "reload",
			disabled: false
		},
		"copy": {
			name: lang.get('clipboardItem'),
			icon: "copy",
			disabled: function(key, options){
				if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"search-google": {
			name: lang.get('searchGoogle'),
			icon: "web-search",
			disabled: function(key, options){
				if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"search-yandex": {
			name: lang.get('searchYandex'),
			icon: "web-search",
			disabled: function(key, options){
				if(!bgpage.player.isPlaying()){
					return true;
				}
				var stitle = $('li[data-alias='+window.options.playStation+']',$('ul')).data('station').title,
					trackTitle = bgpage.trackInfo;
				return (stitle === trackTitle);
			}
		},
		"sep_01":"-----------------",
		"fullscreen" : {
			name: function(){
				return (!document.webkitIsFullScreen) ? lang.get('menufullscreen_enabled') : lang.get('menufullscreen_disabled');
			},
			icon: function(){
				return (!document.webkitIsFullScreen) ? "context-menu-icon context-menu-icon-fullon" : "context-menu-icon context-menu-icon-fulloff";
			}
		},
		"sep_02":"-----------------",
		"reset": {
			name: lang.get('menuReset'),
			icon: "reset"
		}
	},
	optionsContext = {
		selector: 'body, .main, header, .list_wrapper li',
		zIndex: 999999999999,
		build: function($trigger, e) {
			return {
				callback: function(key, options) {
					switch(key){
						case "reload":
							$("#radioext_reload").trigger("click");
							break;
						case "copy":
							copyTrack();
							break;
						case "fullscreen":
						if(!document.webkitIsFullScreen){
								canvas.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
								}else{
								document.webkitCancelFullScreen();
								document.webkitExitFullscreen();
							}
							break;
						case "reset":
							localStorage.clear();
							chrome.runtime.reload();
							break;
						case "search-yandex":
							sendMessage("searchTrack", {search:"yandex"});
							break;
						case "search-google":
							sendMessage("searchTrack", {search:"google"});
							break;
					}
					$('body, .main, header, .list_wrapper li').contextMenu('hide');
				},
				items: contextitems
			};
		}
	},
	viewCount = function(){
		var b = bgpage.count;
		$("#counter").empty();
		(typeof b == "object") && ((b.count > 0 && b.record.count > 0) && $("#counter").text(chrome.i18n.getMessage("online_output_html", [b.count, b.record.count, b.record.date])));
		!options.showCount && $("#counter").addClass("hidden");
	};
	$('.scroll').on("click", "li div .button, li div .logo", clickStation);
	$(window).on('resize', function(){
			api && api.reinitialise(
				{
					contentWidth: 279,
					verticalDragMinHeight: 56
				}
			);
	}).trigger('resize');
	if($("#color").length){
		picker = new jscolor($("#color").disableSelection()[0], {
			hash: true,
			zIndex: 99999999999999999,
			padding:0,
			shadow:false,
			width: 199,
			borderWidth:0,
			backgroundColor:'transparent',
			container: $("#jscolor_container")[0],
			value: window.options.customColor,
			onFineChange: function(){
				var cc = window.options.customColor,
					pc = picker.toHEXString(),
					light = picker.isLight();
				if(cc != pc){
					window.options.customColor = pc;
					window.options.color = light ? "#000" : "#FFF";
					sendPopup({
						name: "popup",
						action: "stylechange",
						data: "undefined" != typeof data ? data : null
					})
				}
			}
		});
		setTimeout(function(){
			$("#color").is(":visible") && picker.show();
		}, 500);
		$(document).on("mousedown", function(e){
			$("#color").is(":visible") && picker.show();
		})
	}
	chrome.runtime.getBackgroundPage(function(page){
		bgpage = page;
		var options = window.options;
		$("title").text(lang.get("name"));
		$(".header_logotip a, .support").attr({
			title: lang.get("gotoradioext")
		});
		$(".header_logotip a").data({
			href: manifest.homepage_url
		});
		$(".support").data({
			href: manifest.homepage_url + "#disqus_thread"
		}).click(function(e){
			e.preventDefault();
			var url = $(this).data("href");
			chrome.tabs.create({
				url: url,
				active: true
			})
			return !1;
		});
		$(".reviews").text(lang.get("reviews")).data({
			href: bgpage.installDom
		}).click(function(e){
			e.preventDefault();
			var url = $(this).data("href");
			chrome.tabs.create({
				url: url,
				active: true
			})
			return !1;
		});
		$("[for=show_panel]").empty().append(chrome.i18n.getMessage("show_panel", "<br>"));
		$("[for=show_info]").text(lang.get("show_info"));
		$("[for=show_count]").text(lang.get("show_count"));
		$(".shareds_item .description").text(lang.get("shared_description"));
		$("#lang_settings").text(lang.get("lang_settings"));
		$(".develop .support").text(lang.get("support"));
		$("#show_panel").prop('checked', options.showPanel).on("input change", function(){
			options.showPanel = this.checked;
		});
		$("#show_info").prop('checked', options.showNotify).on("input change", function(){
			options.showNotify = this.checked;
		});
		$("#show_count").prop('checked', options.showCount).on("input change", function(e){
			e.preventDefault();
			options.showCount = this.checked;
			sendPopup({
				name: "popup",
				action: "showCount",
				data: "undefined" != typeof data ? data : null
			})
			return !1;
		});
		$("#radioext_settings").attr({title: lang.get("lang_settings")});
		$("#lang_settings_skin").text(lang.get("lang_settings_skin"));
		$("*[type=range]").attr({title:lang.get("volumetitle")});
		var skin = options.isSchemeDefault ? options.schemeDefaultIndex : "custom";
		$("#tracking").text(bgpage.trackInfo);
		$("#optskn").on("change", function(e){
			e.preventDefault();
			var val = $(this).val();
			window.options.schemeDefaultIndex = val;
			window.options.isSchemeDefault = (val != "custom");
			(val != "custom") ? (
				$("#color").css({display: "none"}),
				picker.hide()
			) : (
				$("#color").css({display: "block"}),
				picker.show()
			);
			sendPopup({
				name: "popup",
				action: "stylechange",
				data: "undefined" != typeof data ? data : null
			})
			return !1;
		});
		window.awesomplete = new Awesomplete($('#radioext_search')[0],{
			data: function (text, input) {
				return text;
			},
			filter: function(text, input) {
				return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
			},
			maxItems: 6,
			minChars: 1
		});
		$('#radioext_search').on("change input keyup blur focus", function(e){
			sendMessage("search", {search: $(this).val()})
			switch(e.type) {
				case "keyup":
					e.keyCode == 13 && awesomplete.close();
					break;
			}
		});
		$(awesomplete.ul).bind('mousedown', function(){
			sendMessage("search", {search: $('#radioext_search').val()});
		});
		$("#optionPage canvas").on("dblclick", function(e){
			e.preventDefault();
			sendPopup({
				name: "popup",
				action: "fullscreen",
				data: "undefined" != typeof data ? data : null
			});
			return !1;
		})
		$("#radioext_reload").on("click", function(e){
			e.preventDefault();
			sendPopup({
				name: "popup",
				action: "reload",
				data: "undefined" != typeof data ? data : null
			});
			return !1;
		}).attr({
			title: lang.get("reload")
		})
		$("#optionPage").length && $(window).on("webkitfullscreenchange", function(){
			setTimeout(function(){
				if(document.webkitIsFullScreen){
					canvas.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
					w = canvas.width = screen.availWidth;
					h = canvas.height = screen.availHeight+40;
					opts.cx = w * .5;
					opts.cy = h * .5;
					opts.rays = 60;
					opts.maxRadius = Math.sqrt( w*w/2 + h*h/2 );
					startInit();
					$(canvas).attr({title:""});
				}else{
					w = canvas.width = 230;
					h = canvas.height = 40;
					opts.rays = 30;
					opts.cx = w * .5;
					opts.cy = h * .5;
					opts.maxRadius = Math.sqrt( w*w/2 + h*h/2 );
					startInit();
					$(canvas).attr({title:lang.fullscreen});
					
				}
				full = document.webkitIsFullScreen;
			}, 20);
		});
		window.onblur = function(e){
			$('body, .main, header, .list_wrapper li').contextMenu('hide');
		};
		init();
	});
}());
$(function(){
	"use strict";
	var btnClear = $("#search-clear"),
		inputSearch = $("#radioext_search");
	inputSearch.on("change input", function(e){
		var val = $(this).val() + "";
		val = val.replace(/^\s+/, "");
		$(this).val(val);
		val.length ? btnClear.addClass("active") : btnClear.removeClass("active");
	});
	btnClear.on("click", function(e){
		e.preventDefault();
		inputSearch.val("").trigger("change");
		setTimeout(function(){
			inputSearch.focus();
		}, 1);
		return !1;
	});
	$("a, button").on("click", $('.main'), function(e){
		$(this).blur();
	});
	$(".shareds_item .icon").click(function(e){
		e.preventDefault();
		var share = this.classList.value.split(" ")[1].split("-")[1],
			site = bgpage.installDom,
			image = bgpage.installDom + "assets/images/radioext_510x228.jpg",
			title = lang.get("name"),
			desc = lang.get("description"),
			link = "about://about";
		switch(share){
			case "ok":
				link = "https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl="+site;
				break;
			case "vk":
				link = "https://vk.com/share.php?url="+site+"&title="+title+"&image="+image+"&description="+desc;
				break;
			case "fb":
				link = "https://www.facebook.com/sharer.php?s=100&p[url]="+site+"&p[images][0]="+image+"&p[title]="+title+"&p[summary]="+desc;
				break;
			case "tw":
				link = "https://twitter.com/intent/tweet?url="+site+"&text="+desc;
				break;
			case "go":
				link = "https://plus.google.com/share?url="+site+"&hl=ru";
				break;
		}
		chrome.tabs.create({
			url: link,
			active: true
		})
		return !1;
	});
	$("#optionPage #radioext_settings").click(function(){
		$(".extension-set").toggleClass("open")
	});
	$("#actionPage #radioext_settings").click(function(){
		chrome.runtime.openOptionsPage();
	});
});