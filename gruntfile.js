'use strict';
module.exports = function(grunt) {
	require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt);
	
	var pkg = grunt.file.readJSON('package.json'),
		gc = {
			assets 		: 'dest/assets',
			images		: 'dest/assets/images',
			styles		: 'dest/assets/css',
			fonts		: 'dest/assets/fonts',
			scripts 	: 'dest/assets/js',
			locales 	: 'dest/_locales',
			temp 		: 'temp',
			vendors 	: 'src/vendors',
			develop 	: 'src',
			version		: '<%= pkg.version %>',
			zip			: false
		};
	grunt.initConfig({
		gc : gc,
		pkg : pkg,
		// HTML minified
		'htmlmin': {
			dist: {
				options	: {
					removeComments		: true,
					collapseWhitespace	: true
				},
				files	: [
					{
						expand 	: true,
						flatten : true,
						src 	: '<%= gc.develop %>/*.html',
						dest 	: 'dest/',
						filter 	: 'isFile'
					}
				]
			}
		},
		/*
		** JavaScript. Customize jQuery UI sortable
		** + jquery.mousewheel 
		** + jquery.scrollstop 
		** + jquery.lazyload 
		** + jquery.jscrollpane
		/**
		*** Берём только виджет sortable и подключаем всё, что связано с jQuery
		**/
		requirejs: {
			ui: {
				options: {
					baseUrl: __dirname+"/<%= gc.vendors %>/jquery-ui/ui/widgets/",//"./",
					paths: {
						jquery: __dirname+'/<%= gc.vendors %>/jquery/dist/jquery'
					},
					preserveLicenseComments: false,
					optimize: "none",
					findNestedDependencies: true,
					skipModuleInsertion: true,
					exclude: [ "jquery" ],
					include: [ 
								"../disable-selection.js",
								"sortable.js",
							],
					out: "<%= gc.temp %>/js/jquery.vendors.js",
					done: function(done, output) {
						grunt.log.writeln(output.magenta);
						grunt.log.writeln("jQueryUI Custom Build ".cyan + "done!\n");
						grunt.log.writeln("File " + (__dirname +"/" + gc.temp + "/js/jquery.ui.sortable.js").cyan + " created.\n");
						done();
					},
					error: function(done, err) {
						grunt.log.warn(err);
						done();
					}
				}
			}
		},
		concat: {
			options: {
				separator: ';',
			},
			dist: {
				src: [
					'<%= gc.develop %>/js/window.helper.js',
					'<%= gc.develop %>/js/audioplayer.js',
					'<%= gc.develop %>/js/main.js'
				],
				dest: '<%= gc.temp %>/js/main.js'
			},
			popup: {
				src: [
					'<%= gc.vendors %>/awesomplete/awesomplete.js',
					'<%= gc.vendors %>/jquery/dist/jquery.js',
					'<%= gc.temp %>/js/jquery.vendors.js',
					"<%= gc.vendors %>/jScrollPane/script/jquery.mousewheel.js",
					"<%= gc.vendors %>/jquery_lazyload/jquery.scrollstop.js",
					"<%= gc.vendors %>/jquery_lazyload/jquery.lazyload.js",
					"<%= gc.vendors %>/jScrollPane/script/jquery.jscrollpane.js",
					"<%= gc.vendors %>/jQuery-contextMenu/dist/jquery.contextMenu.js",
					"<%= gc.vendors %>/jscolor/jscolor.js",
					'<%= gc.develop %>/js/rays.js',
					'<%= gc.develop %>/js/popup.js',
				],
				dest: '<%= gc.temp %>/js/popup.js'
			}
		},
		// Minimized JavaScript
		uglify: {
				options: {
					//preserveComments: 'some'
					ASCIIOnly: true
				},
				main: {
					src: '<%= gc.temp %>/js/main.js',
					dest: '<%= gc.scripts %>/main.min.js'
				},
				popup: {
					src: '<%= gc.temp %>/js/popup.js',
					dest: '<%= gc.scripts %>/popup.min.js'
				},
				popup_start: {
					src: [
						'<%= gc.develop %>/js/window.helper.js',
						'<%= gc.develop %>/js/start.js'
					],
					dest: '<%= gc.scripts %>/start.js'
				}
		},
		
		//  Unicode Format _locales files
		'unicode': {
			main: {
				
				files: [
					{
						expand: true,
						cwd: '<%= gc.temp %>/_locales/',
						src: ['**/*.json'],
						dest: '<%= gc.locales %>/',
						ext: '.json',
						extDot: 'first'
					}
				]
			}
		},
		// Format _locales files
		'json-format': {
			main: {
				options: {
					indent: "\t",
					remove: ['_comment']
				},
				files: [
					{
						expand: true,
						cwd: '<%= gc.develop %>/_locales/',
						src: ['**/*.json'],
						dest: '<%= gc.temp %>/_locales/',
						ext: '.json',
						extDot: 'first'
					}
				]
			}
		},
		// Generate manifest.json file platforms
		/**
		*** Когда Opera сделает поддержку работы background
		*** это станет не актуальным
		**/
		json_generator: {
			chrome: {
				dest: "dest/manifest.json",
				options: {
					update_url: 		"https://clients2.google.com/service/update2/crx",
					homepage_url: 		"http://radioext.studionions.com/",
					version: 			"<%= pkg.version %>",
					default_locale:		"en",
					options_page: 		"settings.html",
					manifest_version: 	2,
					name: 				"__MSG_name__",
					options_ui: {
						page: "settings.html",
						chrome_style: false,
						open_in_tab: true
					},
					/*"speeddial": {
						"url": "http://radioext.studionions.com/",
						"size_mode": "adapt",
						"title": "Онлайн Радио в твоём браузере | Расширение для браузеров Chrome, Opera, Yandex"
					},*/
					browser_action: {
						default_icon: {
							"19": 		"assets/images/icon19.png",
							"38": 		"assets/images/icon38.png"
						},
						default_title: 	"__MSG_title__",
						default_popup: "index.html"
					},
					commands: {
						reload_stations: {
							suggested_key: {
								default: "Ctrl+Shift+9",
								windows: "Ctrl+Shift+9",
								mac: "Command+Shift+9",
								linux: "Ctrl+Shift+9"
							},
							description: "__MSG_reload__",
							global: true
						},
						playstop: {
							suggested_key: {
								default: "MediaPlayPause",
								windows: "Ctrl+Shift+1",
								mac: "Command+Shift+1",
								linux: "Ctrl+Shift+1"
							},
							description: "__MSG_globPlayStop__",
							global: true
						},
						volumeup: {
							suggested_key: {
								default: "Ctrl+Shift+2",
								windows: "Ctrl+Shift+2",
								mac: "Command+Shift+2",
								linux: "Ctrl+Shift+2"
							},
							description: "__MSG_volumeup__",
							global: true
						},
						volumedown: {
							suggested_key: {
								default: "Ctrl+Shift+3",
								windows: "Ctrl+Shift+3",
								mac: "Command+Shift+3",
								linux: "Ctrl+Shift+3"
							},
							description: "__MSG_volumedown__",
							global: true
						}
					},
					description: 		"__MSG_description__",
					short_name: 		"__MSG_title__",
					icons: {
						"19": 			"assets/images/icon19.png",
						"128": 			"assets/images/icon128.png",
						"38": 			"assets/images/icon38.png",
						"48": 			"assets/images/icon48.png",
						"16": 			"assets/images/icon16.png"
					},
					background: {
						scripts: 		["assets/js/main.min.js"],
						persistent: 	true
					},
					permissions: [
						"background",
						"storage",
						"clipboardRead",
						"clipboardWrite",
						"notifications",
						"*://*/*"
					]
				}
			},
			opera: {
				dest: "dest/manifest.json",
				options: {
					update_url : 'https://extension-updates.opera.com/api/omaha/update/',
					homepage_url: 		"http://radioext.studionions.com/",
					version: 			"<%= pkg.version %>",
					default_locale:		"en",
					options_page: 		"settings.html",
					manifest_version: 	2,
					name: 				"__MSG_name__",
					commands: {
						reload_stations: {
							suggested_key: {
								default: "Ctrl+Shift+9",
								windows: "Ctrl+Shift+9",
								mac: "Command+Shift+9",
								linux: "Ctrl+Shift+9"
							},
							description: "__MSG_reload__"
						},
						playstop: {
							suggested_key: {
								default: "Ctrl+Shift+1",
								windows: "Ctrl+Shift+1",
								mac: "Command+Shift+1",
								linux: "Ctrl+Shift+1"
							},
							description: "__MSG_globPlayStop__",
							global: true
						},
						volumeup: {
							suggested_key: {
								default: "Ctrl+Shift+2",
								windows: "Ctrl+Shift+2",
								mac: "Command+Shift+2",
								linux: "Ctrl+Shift+2"
							},
							description: "__MSG_volumeup__",
							global: true
						},
						volumedown: {
							suggested_key: {
								default: "Ctrl+Shift+3",
								windows: "Ctrl+Shift+3",
								mac: "Command+Shift+3",
								linux: "Ctrl+Shift+3"
							},
							description: "__MSG_volumedown__",
							global: true
						}
					},
					browser_action: {
						default_icon: {
							"19": 		"assets/images/icon19.png",
							"38": 		"assets/images/icon38.png"
						},
						default_title: 	"__MSG_title__",
						default_popup: "index.html"
					},
					description: 		"__MSG_description__",
					short_name: 		"__MSG_title__",
					icons: {
						"19": 			"assets/images/icon19.png",
						"128": 			"assets/images/icon128.png",
						"38": 			"assets/images/icon38.png",
						"48": 			"assets/images/icon48.png",
						"16": 			"assets/images/icon16.png"
					},
					background: {
						scripts: 		["assets/js/main.min.js"],
						persistent: 	true
					},
					permissions: [
						"storage",
						"clipboardRead",
						"clipboardWrite",
						"notifications",
						"*://*/*"
					]
				}
			}
		},
		// Image optimized
		imagemin: {
			dist: {
				options: {
					optimizationLevel: 5,
					svgoPlugins: [
						{
							removeViewBox: false
						}
					]
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: ['<%= gc.develop %>/images/*.{png,jpg,gif,svg}'],
						dest: '<%= gc.develop %>/images/bin/',
						filter : 'isFile'
					}
				]
			}
		},
		// Compile less
		less: {
			dist: {
				files :{
					'<%= gc.styles %>/main.css' : [
							'<%= gc.develop %>/css/global.less'
						]
				},
				options : {
					 compress: true,
					 ieCompat: false
				}
			}
		},
		// Copy file
		copy: {
			fonts: {
				files: [
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/fonts/*.woff',
						dest : '<%= gc.fonts %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/fonts/*.ttf',
						dest : '<%= gc.fonts %>/',
						filter : 'isFile'
					}
				]
			},
			image: {
				files: [
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/ic*.png',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/not*.png',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/*.gif',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/*.jpg',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/lazy*.png',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					},
					{
						expand : true,
						flatten : true,
						src : '<%= gc.develop %>/images/bin/bg.png',
						dest : '<%= gc.images %>/',
						filter : 'isFile'
					}
				]
			}
		},
		// Clean temp, src dir
		clean : {
			default : [
				'*.zip',
				'<%= gc.temp %>/**/*',
				'<%= gc.develop %>/images/bin/*',
				'dest/**/*'
			]
		},
		// Оповещения
		notify: {
			start: {
				options: {
					title: "<%= pkg.description %> v<%= pkg.version %>",
					message: "Старт сборки.\n\nПожалуйста подождите...",
					image: __dirname + "/src/images/notify.png"
				}
			},
			end: {
				options: {
					title: "<%= pkg.description %> v<%= pkg.version %>",
					message: "Проект собран.",
					image: __dirname + "/src/images/notify.png"
				}
			}
		},
		// Изменения файлов
		watch: {
			def : {
				files: [
					'<%= gc.develop %>/js/*.js',
					'<%= gc.develop %>/css/**/*',
					'<%= gc.develop %>/*.html',
					'<%= gc.develop %>/fonts/*',
					'<%= gc.develop %>/images/*',
					'<%= gc.develop %>/_locales/**/*'
				],
				tasks: ['default']
			},
			dev : {
				files: [
					'<%= gc.develop %>/js/*.js',
					'<%= gc.develop %>/css/**/*',
					'<%= gc.develop %>/*.html',
					'<%= gc.develop %>/_locales/**/*',
				],
				tasks: ['develop']
			}
		}
	});
	grunt.registerTask('default', 	[
		'notify:start',
		'clean',
		'htmlmin',
		'json_generator:chrome',
		'json-format',
		'unicode',
		'requirejs',
		'concat',
		'uglify',
		'imagemin',
		'less',
		'copy',
		'notify:end'
	]);
	
	grunt.registerTask('develop', 	[
		'notify:start',
		'htmlmin',
		'json_generator:chrome',
		'json-format',
		'unicode',
		'requirejs',
		'concat',
		'uglify',
		'less',
		'copy',
		'notify:end'
	]);
	
	grunt.registerTask('dev', 	[
		'watch:dev'
	]);
	
	/**
	*** Running expansion assembly for the Opera browser
	**/
	grunt.registerTask('opera', 	[
		'notify:start',
		'clean',
		'htmlmin',
		'json_generator:opera',
		'json-format',
		'unicode',
		'requirejs',
		'concat',
		'uglify',
		'imagemin',
		'less',
		'copy',
		'notify:end'
	]);
}